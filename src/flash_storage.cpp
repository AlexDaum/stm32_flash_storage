/*
 *stm32_flash_storage
 *Copyright (C) 2021  Alex Daum
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 *USA
 */

#include "flash_storage/flash_storage.hpp"
#include "flash_storage/pages.hpp"
#include "stm.h"
#include <bitset>

using namespace flash_storage;

FlashStorage FlashStorage::m_instance;

VirtualPage *flash_area =
	reinterpret_cast<VirtualPage *>(cfg::flash_storage_start);

size_t activePage = 0;
bool activePageValid = false;
uint16_t writeIdx;

static VirtualPage &getActivePage() { return flash_area[activePage]; }

static void erase_page(uintptr_t ptr_to_page);
static void write_flash(uint16_t *addr, uint16_t value);
static void look_for_active_page();

__attribute__((noinline)) static void error() {
	while (1) {
		__BKPT();
	}
}

void VirtualPage::erase() {
	uint16_t *ptr = storage.data();
	for (size_t i = 0; i < cfg::virtual_page_size; i++) {
		erase_page(reinterpret_cast<uintptr_t>(ptr));
		ptr += cfg::page_size_hw;
	}
}

void VirtualPage::status(enum PageStatus newStatus) {
	write_flash(reinterpret_cast<uint16_t *>(&this->m_status),
				static_cast<uint16_t>(newStatus));
}

void VirtualPage::write(size_t index, uint16_t data) {
	write_flash(&storage[index], data);
}

const uint16_t *FlashStorage::getPtrToPageIndex(index_t &idx, id_t id) {
	if (idx.index == index_t::invalid_idx)
		return nullptr;
	if (gc_count != idx.gc_count) {
		// This index is invalid, load a new one
		idx = load(id);
	}
	// Offset 2Byte!
	return &(getActivePage()[idx.index]) + 1;
}

index_t FlashStorage::load(id_t id) {
	if (!activePageValid) {
		look_for_active_page();
	}

	index_t latest{index_t::invalid_idx, gc_count};

	uint16_t index = 0;
	const VirtualPage &page = getActivePage();
	while (1) {
		// This should always be a header if present
		auto header = page.asHeader(index);
		// If 0xFFFF then we hit an erased header, finished
		if (header == 0xFFFF) {
			break;
		}
		if (header.id == id) {
			latest.index = index;
		}
		index += header.size + 1;
		if (index >= page.size()) {
			break;
		}
	}

	return latest;
}

index_t FlashStorage::store(id_t id, std::size_t obj_size,
							const void *ptr_to_data) {

	// Sanity check size: 8bit size for Halfword, so max 2^8 * 2 Byte = 512Byte
	if (obj_size > 512) {
		error();
	}

	// Check remaining size:
	std::size_t remaining = getActivePage().size() - writeIdx;
	std::size_t size = (obj_size + 1) / 2;
	// Needed Size = SizeByte/2 (round up) + 1 for header
	std::size_t needed = size + 1;

	if (needed > remaining) {
		// Need to garbage collect
		garbage_collect();
	}
	// Assemble Header
	Header header{id, static_cast<uint8_t>(size)};
	index_t indexWrittenTo{writeIdx, gc_count};
	getActivePage().write(writeIdx++, header);
	// Write Data
	const uint8_t *byte_data = reinterpret_cast<const uint8_t *>(ptr_to_data);
	for (size_t i = 0; i < size; i++) {
		uint16_t nextData = byte_data[2 * i];
		if (2 * i + 1 < obj_size) {
			nextData |= (byte_data[2 * i + 1]) << 8;
		}
		getActivePage().write(writeIdx++, nextData);
	}
	return indexWrittenTo;
}

void FlashStorage::garbage_collect() {
	// Erase the new page
	VirtualPage &oldPage = getActivePage();
	uint16_t newPageIdx = (activePage + 1) % cfg::n_pages;
	VirtualPage &newPage = getActivePage();
	// The current page is full, so we need to copy data to the next page. The
	// next page has to be erased beforehand
	getActivePage().status(PageStatus::FULL);
	if (newPage.status() != PageStatus::ERASED) {
		newPage.erase();
	}

	// We care most about memory consumption, so we try to use as little as
	// possible at the same time. Runtime is not as important, especially since
	// the flash erase/program itself is already a slow process

	// there are 256 possible ids, in order to speed up the search a bit, we
	// keep a bitmap of all ids that were already encountered, so we can skip
	// those quicker.

	std::bitset<256> idmap;

	uint16_t index = 0;
	uint16_t newIndex = 0;
	while (index < oldPage.size()) {
		auto &header = oldPage.asHeader(index++);
		uint16_t currentID = header.id;
		index += header.size;
		if (idmap[currentID]) {
			// Already processed, just skip
			continue;
		}
		// Now we get the newest value for that id
		uint16_t latestIdx = load(currentID).index;
		if (latestIdx == index_t::invalid_idx) {
			error();
		}
		// And copy all data to the new page
		// The header is the same, so simply copy all data
		const uint16_t *itr = oldPage.at(latestIdx + 1);
		for (size_t i = 0; i < header.size; i++) {
			newPage.write(newIndex++, *itr);
			itr++;
		}
		idmap[currentID] = true;
	}

	// Now all important data is copied to the new page, so switch pages
	gc_count++;
	activePage = newPageIdx;
	writeIdx = newIndex;
}

static void unlock_flash() {
	FLASH->KEYR = 0x45670123;
	FLASH->KEYR = 0xCDEF89AB;
}

static void erase_page(uintptr_t ptr_to_page) {
	if (FLASH->CR & FLASH_CR_LOCK_Msk) {
		unlock_flash();
	}
	FLASH->CR = FLASH_CR_PER_Msk;
	FLASH->AR = static_cast<uint32_t>(ptr_to_page);
	FLASH->CR |= FLASH_CR_STRT_Msk;
	while (FLASH->SR & FLASH_SR_BSY_Msk) {
	}
	// confirm erase
	// Check at least first word (word-aligned access)
	const uint32_t *ptr =
		reinterpret_cast<const uint32_t *>(ptr_to_page & (0xFFFFFFFC));
	if (*ptr != 0xFFFFFFFF) {
		error();
	}
}

static void write_flash(uint16_t *addr, uint16_t value) {
	if (FLASH->CR & FLASH_CR_LOCK_Msk) {
		unlock_flash();
	}
	FLASH->CR |= FLASH_CR_PG_Msk;
	*addr = value;
	while (FLASH->SR & FLASH_SR_BSY_Msk) {
	}
}

static void look_for_active_page() {
	for (unsigned idx = 0; idx < cfg::n_pages; idx++) {
		const VirtualPage &vp = flash_area[idx];
		if (vp.status() == PageStatus::IN_USE) {
			activePage = idx;
			activePageValid = true;
			uint16_t i = 0;
			while (1) {
				if (getActivePage().isIndexFFFF(i))
					break;
				auto &header = getActivePage().asHeader(i);
				i += header.size + 1;
			}
			writeIdx = i;
			return;
		}
	}
	if (!activePageValid) {
		// No Page in use, all either erased or full. Use first page
		activePage = 0;
		if (getActivePage().status() != PageStatus::ERASED)
			getActivePage().erase();
		writeIdx = 0;
		getActivePage().status(PageStatus::IN_USE);
	}
}
