# Overview
I wrote this library to allow easy storage of non-volative data in the Flash of STM32 devices.
This library probably works with C++ 11 and newer, but I only tested it with C++17, because thats what I use for my own projects.

Also please note, that this library is not built to be very save, it is very likely, that random power loss could result in lost data or even corruption of the complete memory region.

Also this is still not a final version of code, that I'm happy with, so it will get at least one major refactoring.

## Usage
### Including the library
In order to use this library, copy or link the contents of the inc folder to a directory named "flash_storage" in your include path and copy or link the contents of the src folder somewhere in your project, so they get compiled and linked to the final binary.

### Configuration
There are some things you will have to configure for the library to work on your device. Almost all configuration is done in the file flash_storage_config.hpp.

Following variables are available:

* flash_storage_start: the start address of the memory region used to store data
* page_size_hw: the size of a flash page in halfwords
* n_pages: the number of pages used
* virtual_page_size: how many real pages are combined to result in a virtual page

#### How to determine this variables:
This part describes how to figure out the values for most configuration variables and includes example values for an STM32F051x6


**Figuring out page_size_hw:**  
You will first have to figure out how the flash is organized on your Chip, for STM32 devices, this information is typically found in the Reference Manual (the chapter is called flash->functional description, or similar).  
There you can find the size the individual pages of flash, which may vary for different size variants of the same family.  
For the STM32F0 Family, we find that low- and medium-density devices have 1kB pages, while high-, very-high-density and connectivity-line devices have 2kB pages.
The STM32F051x6 is a low-density device, so it has 1kB pages. For the configuration variable, we have to enter the value as halfwords (16-bit), so it is set to 512.

**Figuring out n_pages:**
n_pages should most times be set to "2", other values may be useful if you want to increase endurance and don't need to store larger Objects.

**Figuring out virtual_page_size:**  
This is the main parameter, that is used to configure the available storage for data.  
The amount of available storage S in halfwords is given by following equation: `S = (page_size_hw * virtual_page_size - 1)`. The reason for this is described further down in the Technical Description.

In order to figure out how much storage you need, convert the size of all your configuration variables into halfwords (round up) to get the data portion. Aditionally each individual variable will also have an overhead of 1 Halfword for header-data.

As an example, we take an application, that needs to store 3 parameters, call them I, D and P, each a 32-bit integer. So each variable is 2 Halfwords in size + 1 Halfword of header data. This results in a total of 9 Halfwords of needed space, so we simply choose virtual_page_size = 1, which gives S = 511HW.

There will be significant write amplification if the needed storage is a larger than about S/2.

**figuring out flash_storage_start:**
If you have all other variables, you can figure out where the storage area has to begin.
I recommend putting this data at the end of your flash, but if you can put it anywhere you want (if there is a block of unused flash there).  
I also recommend modifying the linker script, so the flash_storage section will not accidentally used by different data.

First calculate the total size of the region in Bytes: `page_size_hw * 2 * virtual_page_size * n_pages`.  In the example this is `512 * 2 * 1 * 2 = 2048 = 0x800` 
Then figure out the end address of the flash, since flash starts at `0x0800 0000`, we add the flash size to that. For the 32kB STM32F051x6 this results in `0x0800 8000`.  
From this we then subtract the size of the region, for the example this results in a start Adress of `0x0800 7800`

Now we also know the region used by this library, we can reduce the modify the linker script (if you use STM32CubeIDE, this file will be called DEVICE_NAME_FLASH.ld).

At the beginning of the linker script, memory regions will be defined, one of those will start at `0x0800 0000` (most likely called FLASH or ROM).  
To account for the space used by this library, simply reduce the LENGTH attribute by the space used, in our example this is originally 32K, which will be reduced to 30K.

### Finally using the library

For finally using the library, you need to assign each separate value you want to store an 8-bit numeric ID.

#### Creating a Variable
Then you can create a configuration value by declaring a global Variable of type:
`flash_storage::FlashObject<type, ID>`  
`type` has to be trivially copyable for this to work

The object can either be default constructed (it will have an invalid value if it was never written) or constructed using a constructor with a default value, that will be assigned if there is no value with this ID.

If you want to change the Datatype of a configuration value, which has already been written to on a device, you HAVE to do a complete flash erase before programming, not every programmer does that by default.

#### Accessing the Value
In order to access the value, the dereferencing operator `*` is used, which returns a const reference.

e.g.:

```c++
flash_storage::FlashObject<uint32_t, 0> I(500);
//Accessing I
const uint32_t &value = *I;
```

#### Chaning the Value
In order to write to the flash, the `=` operator is used, which takes a const reference.

e.g.
```c++
flash_storage::FlashObject<uint32_t, 0> I(500);
//Modifying I
I = 899
```

#### Speed of access

Reading a value will be reasonably fast if no page garbage collection has happened since the last access, but it still is some pointer arithmetic, so prefer to load the value to a temporary variable if you need to read it often in a method.

Writing is not very fast, since programming flash is a slow process, and the write is dominated by this speed. Most writes will only have to write data to the flash, the speed of this is detailed in the datasheet of your controller. For the STM32F051 this is Typ 53us per Halfword, so for a 32bit integer + header it is 150us (1200 cycles if running at 8MHz).

Sometimes a write will run out of space on the currently active page, which means a garbage collection has to be performed. This also has to erase a full virtual page, which will take quite a long time.

Again for the STM32F051 page erase time is 20 - 40ms, for the virtual page erase time, this has to be multiplied by virtual_page_size.

## Technical Description
TODO
