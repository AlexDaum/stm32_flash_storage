/*
 *stm32_flash_storage
 *Copyright (C) 2021  Alex Daum
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 *USA
 */
#pragma once

#include "flash_storage_config.hpp"
#include <array>
#include <cstdint>

namespace flash_storage {

/*
 * Structure of a Field:
 * All fields are Structured like this:
 * 16bit Header
 * n*16bit data
 *
 * The size of data is determined by n, which is the size field in the header.
 *
 * The size field is 8 bit, so the largest object that can be stored is 512B in
 * size.
 */

struct Header {
	// The ID
	uint16_t id : 8;
	// The Size in halfwords
	uint16_t size : 8;

	constexpr Header(uint16_t v) : id((v & 0xFF00) >> 8), size(v & 0x00FF) {}
	constexpr Header(id_t p_id, uint16_t p_size) : id(p_id), size(p_size) {}

	operator uint16_t() const { return id << 8 | size; }
};

enum class PageStatus : uint16_t {
	// We can always write a 0 to any value
	FULL = 0,
	IN_USE = 0x00AA,
	// An erased PAGE will only contain 0xFFFF
	ERASED = 0xFFFF
};

/*
 * Defines a virtual page. Each virtual Page has size of virtual_page_size +
 * page_size, but the first Half Word is not useable, since it is used for
 * marking used/unused pages/garbage collected
 */
class VirtualPage {

  public:
	using array_t =
		std::array<uint16_t, cfg::virtual_page_size * cfg::page_size_hw - 1>;
	static constexpr void check() {
		static_assert(sizeof(VirtualPage) == cfg::virtual_page_size *
												 cfg::page_size_hw *
												 sizeof(uint16_t));
	}

	void erase();
	const uint16_t &operator[](size_t index) const { return storage[index]; }
	constexpr size_t size() const { return storage.size(); }

	const uint16_t *at(size_t index) const { return &storage[index]; }

	bool isIndexFFFF(size_t index) const { return storage[index] == 0xFFFF; }

	const Header &asHeader(size_t index) const {
		return reinterpret_cast<const Header &>(storage.at(index));
	}

	const array_t &data() { return storage; }

	void write(size_t index, uint16_t data);

	const PageStatus &status() const { return m_status; }
	void status(PageStatus newStatus);

  private:
	PageStatus m_status;
	array_t storage;
};

} // namespace flash_storage
