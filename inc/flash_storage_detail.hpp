/*
 *stm32_flash_storage
 *Copyright (C) 2021  Alex Daum
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 *USA
 */
#pragma once
#include "flash_storage.hpp"

namespace flash_storage {

template <typename T, id_t ID> FlashObject<T, ID>::FlashObject() {
	idx = FlashStorage::instance().load(ID);
}

template <typename T, id_t ID>
FlashObject<T, ID>::FlashObject(const T &default_val) {
	idx = FlashStorage::instance().load(ID);
	if (idx.index == index_t::invalid_idx) {
		idx = FlashStorage::instance().store(ID, sizeof(T), &default_val);
	}
}

template <typename T, id_t ID>
void FlashObject<T, ID>::operator=(const T &newVal) {
	idx = FlashStorage::instance().store(ID, sizeof(T), &newVal);
}

template <typename T, id_t ID> const T &FlashObject<T, ID>::operator*() {
	return *FlashStorage::instance().get<T>(*this);
}

} // namespace flash_storage
