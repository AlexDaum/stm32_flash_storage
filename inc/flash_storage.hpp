/*
 *stm32_flash_storage
 *Copyright (C) 2021  Alex Daum
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 *USA
 */
#pragma once
#include <cstdint>
#include <type_traits>

namespace flash_storage {

using id_t = uint8_t;

/*
 * Combines an index into the Page with a GC-Number to reload when a GC is
 * performed
 */
struct index_t {
	bool valid() const;
	uint16_t index;
	uint16_t gc_count;

	static constexpr uint16_t invalid_idx = 0xFFFF;
};

/*
 * Represents a variable, that is stored in flash. The type must be
 * trivially copyable (e.g. primitive types)
 */

class FlashStorage;

template <typename T, id_t ID> class FlashObject {
	friend FlashStorage;

  public:
	constexpr FlashObject(const FlashObject &o) = default;
	constexpr FlashObject &operator=(const FlashObject &o) = default;

	explicit FlashObject(const T &default_val);

	FlashObject();

	/*
	 * Accessing empty FlashObject will cause same behavior as null
	 * dereferencing
	 */
	const T &operator*();

	bool valid() const { return idx.valid(); }

	operator bool() const { return valid(); }

	void operator=(const T &newVal);

  private:
	index_t idx;
};

/*
 * Singleton instance of the FlashStorage, has to be used to create new Managed
 * Objects.
 */
class FlashStorage {
	template <typename T, id_t ID> friend class FlashObject;

  public:
	static FlashStorage &instance() { return m_instance; };

	template <typename T, id_t ID> const T *get(FlashObject<T, ID> &obj) {
		return reinterpret_cast<const T *>(getPtrToPageIndex(obj.idx, ID));
	}

  private:
	index_t store(id_t id, std::size_t obj_size, const void *ptr_to_data);

	index_t load(id_t id);

	void garbage_collect();

	const uint16_t *getPtrToPageIndex(index_t &idx, id_t id);

	uint16_t gc_count = 0;

	static FlashStorage m_instance;
};
} // namespace flash_storage

#include "flash_storage_detail.hpp"
