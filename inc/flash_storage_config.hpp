/*
 *stm32_flash_storage
 *Copyright (C) 2021  Alex Daum
 *
 *This library is free software; you can redistribute it and/or
 *modify it under the terms of the GNU Lesser General Public
 *License as published by the Free Software Foundation; either
 *version 2.1 of the License, or (at your option) any later version.
 *
 *This library is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *Lesser General Public License for more details.
 *
 *You should have received a copy of the GNU Lesser General Public
 *License along with this library; if not, write to the Free Software
 *Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 *USA
 */

#pragma once

#include <cstdint>
#include <cstdlib>

namespace flash_storage {
namespace cfg {

/*
 *Name of the section, where the data should be stored. This has to be declared
 *in the linker script
 */
//#define FLASH_STORAGE_SECTION_NAME ".flash_storage"

/**
 * Adress where the flash_storage begins
 *
 * The Area flash_storage_start to flash_storage_start +
 * n_pages*virtual_page_size*page_size_hw*2 must not be used for other data and
 * should probably not be included in the linker script
 */
constexpr std::uintptr_t flash_storage_start = 0x08007800;

/*
 * Size of a single Page, this library only supports pages of equal size
 * Size is given in HALFWORDS
 */
constexpr std::size_t page_size_hw = 512;

/*
 * Count of virtual pages used. Must be at least 2. More Pages do NOT increase
 * available storage, but only increase write endurance.
 *
 * If you need more storage, increase virtual_page_size
 */
constexpr std::size_t n_pages = 2;

/*
 * How many physical pages are connected to be a virtual page. Larger virtual
 * pages allow more values to be stored, but performance of first access can be
 * reduced
 */
constexpr std::size_t virtual_page_size = 1;

} // namespace cfg
} // namespace flash_storage
